import 'package:drone_game/screens/game/game.dart';
import 'package:flutter/material.dart';

class DroneItem extends StatefulWidget {
  const DroneItem({
    Key? key,
    required this.drone,
    required this.isSelected,
    required this.onTap,
  }) : super(key: key);

  final Drone drone;

  final bool isSelected;

  final VoidCallback onTap;

  @override
  _DroneItemState createState() => _DroneItemState();
}

class _DroneItemState extends State<DroneItem> {
  Color? color;

  Drone get drone => widget.drone;

  @override
  Widget build(BuildContext context) {
    final backgroundColor = Theme.of(context).dividerColor;
    return GestureDetector(
      onTap: !widget.isSelected ? widget.onTap : null,
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        onHover: (_) {
          setState(() => color = backgroundColor.withOpacity(.5));
        },
        onExit: (_) => setState(() => color = null),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
          color: widget.isSelected ? backgroundColor : color,
          child: Row(
            children: [
              Container(
                  padding: const EdgeInsets.only(right: 12),
                  child: const Icon(Icons.precision_manufacturing)),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      drone.name,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    const Text('AI: Custom', style: TextStyle(fontSize: 14)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
