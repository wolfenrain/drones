import 'package:code_text_field/code_text_field.dart';
import 'package:drone_game/screens/game/game.dart';
import 'package:drone_game/screens/game/hetu_script.dart';
import 'package:flutter/material.dart';
import 'package:flutter_highlight/themes/atom-one-dark.dart';

class CodeView extends StatefulWidget {
  const CodeView({Key? key, required this.world}) : super(key: key);

  static const listWidth = 240.0;

  final World world;

  @override
  _CodeViewState createState() => _CodeViewState();
}

class _CodeViewState extends State<CodeView> {
  CodeController? _codeController;

  Drone? _drone;

  World get world => widget.world;

  final Map<String, TextStyle> _theme = atomOneDarkTheme;

  @override
  void dispose() {
    _codeController?.dispose();
    super.dispose();
  }

  void _uploadScript() {
    _drone?.script = _codeController!.text;
  }

  TextSpan lineNumberBuilder(int currentLine, TextStyle? style) {
    final text = _codeController!.text;
    final lines = text.split('\n');
    final start = _codeController!.value.selection.start;

    var length = 0;
    var line = 0;
    for (; line < lines.length; line++) {
      length += lines[line].length + 1;
      if (start < length) {
        break;
      }
    }
    return TextSpan(
      text: '$currentLine',
      style: style?.copyWith(
        color: line + 1 == currentLine ? Colors.white : null,
      ),
    );
  }

  void selectDrone(Drone drone) {
    world.selectedDrone = drone;
    setState(() {
      _drone = drone;
      _codeController = CodeController(
        text: drone.script,
        language: hetuScript,
        theme: _theme,
        modifiers: [
          const CurlyBlockModifier(),
        ],
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: _theme['root']?.color,
      body: Row(
        children: [
          Container(
            width: CodeView.listWidth,
            color: const Color(0xFF141a1f),
            child: ListView.builder(
              itemCount: world.drones.length,
              itemBuilder: (context, index) {
                return DroneItem(
                  drone: world.drones[index],
                  isSelected: _drone == world.drones[index],
                  onTap: () => selectDrone(world.drones[index]),
                );
              },
            ),
          ),
          if (_codeController != null)
            Expanded(
              child: Scaffold(
                body: CodeField(
                  controller: _codeController!,
                  expands: true,
                  textStyle: const TextStyle(fontFamily: 'SourceCode'),
                  lineNumberBuilder: lineNumberBuilder,
                ),
                floatingActionButton: _drone != null
                    ? FloatingActionButton(
                        child: const Icon(Icons.upload_file),
                        onPressed: _uploadScript,
                      )
                    : null,
              ),
            ),
        ],
      ),
    );
  }
}
