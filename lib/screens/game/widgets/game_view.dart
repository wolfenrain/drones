import 'package:drone_game/screens/game/game.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';

class GameView extends StatelessWidget {
  const GameView({Key? key, required this.world}) : super(key: key);

  final World world;

  @override
  Widget build(BuildContext context) {
    return GameWidget(game: world);
  }
}
