import 'package:drone_game/screens/game/game.dart';
import 'package:flutter/material.dart';

class GamePage extends StatefulWidget {
  const GamePage({Key? key}) : super(key: key);

  @override
  State<GamePage> createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {
  static const barWidth = 12.0;

  double width = CodeView.listWidth;

  GlobalKey key = GlobalKey();

  final world = World();

  Offset? tappedPosition;

  void _onTapDown(TapDownDetails event) {
    tappedPosition = event.localPosition;
  }

  void _onPanUpdate(DragUpdateDetails event) {
    final renderBox = key.currentContext!.findRenderObject() as RenderBox;

    final localPosition = renderBox.globalToLocal(event.globalPosition);

    var newWidth = (localPosition.dx - (tappedPosition?.dx ?? 0));

    if (newWidth > renderBox.size.width - CodeView.listWidth) {
      newWidth = renderBox.size.width;
    } else if (newWidth < CodeView.listWidth * 2) {
      newWidth = CodeView.listWidth;
    }

    newWidth =
        newWidth.clamp(0, renderBox.size.width - barWidth).roundToDouble();

    if (newWidth % 2 == 1) {
      newWidth += 1;
    }

    setState(() => width = newWidth);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        key: key,
        children: [
          Positioned.fill(child: GameView(world: world)),
          Positioned(
            top: 0,
            left: 0,
            bottom: 0,
            width: width,
            child: SizedBox(width: width, child: CodeView(world: world)),
          ),
          Positioned(
            top: 0,
            left: width,
            bottom: 0,
            width: barWidth,
            child: GestureDetector(
              onTapDown: _onTapDown,
              onPanUpdate: _onPanUpdate,
              child: Container(
                width: barWidth,
                color: Theme.of(context).dividerColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
