import 'dart:math';

import 'package:drone_game/screens/game/world/world.dart';
import 'package:flame/components.dart';
import 'package:flame/extensions.dart';
import 'package:flutter/material.dart';

/// Display information of a component in a standarized way.
mixin Info on HasGameRef<World>, PositionComponent {
  TextPaint get infoTextPaint;

  Paint get infoPaint;

  Map<String, dynamic> infoText();

  bool get displayInfo;

  String get name;

  @override
  void render(Canvas canvas) {
    super.render(canvas);
    if (!displayInfo) {
      return;
    }

    final double radius;
    if ((size.x / 2) + 25 < 50) {
      radius = 50.0;
    } else {
      radius = size.x / 2 + 25;
    }

    final v2 = gameRef.camera.follow!;
    final v1 = center;
    final angle = atan2(v2.y - v1.y, v2.x - v1.x);

    final absAngle = angle.abs();
    final int between;
    // Check if angle is between 90 and 180
    if (absAngle > 1.570796 && absAngle < 3.141593) {
      between = -1;
    } else {
      between = 1;
    }

    final info = {
      'Name': name,
      ...infoText(),
      'Position': '[${center.x.toInt()}, ${center.y.toInt()}]',
    };

    final keyLength = info.keys.fold<int>(0, (p, e) {
      final length = infoTextPaint.measureTextWidth('$e:').toInt() + 6;
      return p < length ? length : p;
    });
    final valueLength = info.values.fold<int>(0, (p, e) {
      final length = infoTextPaint.measureTextWidth('$e').toInt();
      return p < length ? length : p;
    });

    final text = [
      for (final item in info.entries)
        '${item.key.padRight(keyLength)} ${item.value}',
    ].join('\n');

    final textOffset = (keyLength + valueLength) * between;
    final corner = center + Vector2(cos(angle) * radius, sin(angle) * radius);
    final endPos = corner.clone()..x += textOffset;
    final spacing = 2 * (between == 1 ? 1 : -1);

    canvas
      ..save()
      ..transform(Matrix4.inverted(transformMatrix).storage)
      ..drawPath(
        Path()
          ..moveTo(center.x, center.y)
          ..lineTo(corner.x, corner.y)
          ..lineTo(endPos.x + spacing, endPos.y),
        infoPaint,
      );

    final textPosition = Vector2.copy(between == 1 ? corner : endPos)
      ..y -= infoTextPaint.measureTextHeight(text) + 2
      ..x += spacing / 2;

    infoTextPaint.render(
      canvas,
      info.keys.map((e) => '$e:').join('\n'),
      textPosition,
    );
    infoTextPaint.render(
      canvas,
      info.values.join('\n'),
      textPosition..x += keyLength,
    );
    canvas.restore();
  }
}
