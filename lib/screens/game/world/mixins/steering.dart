import 'dart:math';

import 'package:flame/components.dart';
import 'package:drone_game/screens/game/game.dart';

mixin Steering on PositionComponent {
  var velocity = Vector2.zero();
  var mass = 1.0;
  var maxSpeed = 50.0;

  var maxForce = 5.0;
  var arrivalThreshold = 400.0;

  var wanderAngle = 0.0;
  var wanderDistance = 10.0;
  var wanderRadius = 5.0;
  var wanderRange = 1.0;

  var avoidDistance = 50;
  var maxAvoidForce = 100.0;

  var inSightDistance = 200;
  var tooCloseDistance = 60;

  var steeringForce = Vector2.zero();

  final List _velocitySamples = [];
  int numSamplesForSmoothing = 20;

  void lookAt(Vector2 direction) {
    final v2 = direction;
    final v1 = center;
    angle = atan2(v1.y - v2.y, v1.x - v2.x) - (degrees2Radians * 90);
  }

  void lookWhereGoing({bool smoothing = false}) {
    final ownPosition = absolutePosition;

    var direction = ownPosition.clone()..add(velocity);

    if (smoothing) {
      if (_velocitySamples.length == numSamplesForSmoothing) {
        _velocitySamples.removeAt(0);
      }

      _velocitySamples.add(velocity.clone());
      direction.setZero();
      for (var v = 0; v < _velocitySamples.length; v++) {
        direction.add(_velocitySamples[v]);
      }
      direction.divideScalar(_velocitySamples.length.toDouble());
      direction = ownPosition.clone()..add(direction);
    }
    lookAt(direction);
  }

  void seek(Vector2 position) {
    final desiredVelocity = position.clone()
      ..sub(absolutePosition)
      ..normalize();

    desiredVelocity
      ..setLength(maxSpeed)
      ..sub(velocity);

    steeringForce.add(desiredVelocity);
  }

  void wander() {
    var center = velocity.clone()
      ..normalize()
      ..setLength(wanderDistance);

    var offset = Vector2.all(1);
    offset.setLength(wanderRadius);
    offset.x = cos(wanderAngle) * offset.length;
    offset.y = sin(wanderAngle) * offset.length;

    wanderAngle += Random().nextDouble() * wanderRange - wanderRange * .5;
    center.add(offset);
    steeringForce.add(center);
  }

  void idle() {
    velocity.setZero();
    steeringForce.setZero();
  }

  void avoid(Iterable<PositionComponent> obstacles) {
    final dynamicLength = velocity.length / maxSpeed;
    final ahead = absolutePosition.clone()
      ..add((velocity.clone()..normalize()) * (dynamicLength));
    final ahead2 = absolutePosition.clone()
      ..add((velocity.clone()..normalize()) * (avoidDistance * .5));

    final ownPosition = absolutePosition;

    // Get most threatening.
    PositionComponent? mostThreatening;
    for (final obstacle in obstacles) {
      if (obstacle == this) {
        continue;
      }
      final obstaclePosition = obstacle.absolutePosition;
      final radius = obstacle.size.x / 2 + size.x;
      final collision =
          obstaclePosition.distanceToSquared(ahead) <= radius * radius ||
              obstaclePosition.distanceToSquared(ahead2) <= radius * radius;
      if (collision &&
          (mostThreatening == null ||
              ownPosition.distanceToSquared(obstaclePosition) <
                  ownPosition
                      .distanceToSquared(mostThreatening.absolutePosition))) {
        mostThreatening = obstacle;
      }
    }

    final avoidance = Vector2.zero();
    if (mostThreatening != null) {
      avoidance.setFrom(
        (ahead.clone()..sub(mostThreatening.absolutePosition)).normalized() *
            maxAvoidForce,
      );
    }
    steeringForce.add(avoidance);
  }

  @override
  void update(double dt) {
    steeringForce.clampLength(0, maxForce);
    steeringForce / mass;
    if (velocity.isZero()) {
      velocity.add(steeringForce * dt);
    } else {
      velocity.add(steeringForce * dt);
    }
    steeringForce.setZero();

    velocity.clampLength(0, maxSpeed);

    position.add(velocity * dt);

    super.update(dt);
  }
}
