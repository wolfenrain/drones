import 'package:drone_game/screens/game/game.dart';
import 'package:flame/components.dart';
import 'package:flame/input.dart';
import 'package:flutter/material.dart';

final _selectPaint = NeonComponent.paint(Colors.white.withOpacity(.5));

mixin Selectable on PositionComponent, HasGameRef<World>, Tappable {
  bool get isSelected => gameRef.selected == this;

  Paint get selectPaint => _selectPaint;

  @override
  bool onTapDown(TapDownInfo info) {
    gameRef.selected = this;
    return false;
  }
}
