import 'dart:typed_data';

import 'package:drone_game/bindings/bindings.dart';
import 'package:drone_game/screens/game/game.dart';
import 'package:flame/components.dart';
import 'package:flutter/services.dart';
import 'package:hetu_script/hetu_script.dart';

class ScriptComponent extends Component with HasGameRef<World> {
  ScriptComponent({required String script}) {
    _script = script;
  }

  static final _modules = <String, Uint8List>{};

  static Future<void> _compile(String name) async {
    _modules[name] = Hetu().compile(
      await rootBundle.loadString('assets/modules/$name.ht'),
    )!;
  }

  late String _script;

  late Hetu _hetu;

  String get script => _script;

  set script(String value) {
    _script = value;

    _hetu = Hetu();
    _hetu.init(externalClasses: [
      DroneBinding(),
      Vector2Binding(),
      NotifyingVector2Binding(),
      ResourceClusterBinding(),
      ResourceBinding(),
    ]);
    for (final module in _modules.entries) {
      _hetu.loadBytecode(bytes: module.value, moduleName: module.key);
    }

    _hetu.eval(_script, isStrictMode: true);
  }

  @override
  Future<void> onLoad() async {
    assert(parent is Drone, 'ScriptComponents can only be added to Drones');
    if (_modules.isEmpty) {
      await Future.wait([
        _compile('math'),
        _compile('core'),
      ]);
      script = _script;
    }
  }

  void onProcessTick() {
    _hetu.invoke('process', positionalArgs: [parent]);
  }
}
