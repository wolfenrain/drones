import 'dart:ui';

import 'package:flame/components.dart';
import 'package:flame/extensions.dart';
import 'package:flame/geometry.dart';

class NeonComponent extends PositionComponent {
  NeonComponent({
    required Vector2 position,
    required Vector2 size,
    required Color color,
    double strokeWidth = 2,
    HitboxShape? shape,
    double? angle,
    int? priority,
  })  : neonPaint = paint(color, strokeWidth: strokeWidth),
        super(
          position: position,
          size: size,
          priority: priority,
          angle: angle,
          anchor: Anchor.center,
        ) {
    this.shape = shape;
  }

  static Paint paint(Color color, {double strokeWidth = 2.0}) {
    return Paint()
      ..color = color
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth
      ..maskFilter = const MaskFilter.blur(BlurStyle.solid, 5)
      ..strokeCap = StrokeCap.round;
  }

  final Paint neonPaint;

  HitboxShape? _shape;

  HitboxShape? get shape => _shape;

  set shape(HitboxShape? shape) {
    shape?.component = this;
    _shape = shape;
  }

  @override
  void render(Canvas canvas) {
    super.render(canvas);
    _shape?.render(canvas, neonPaint);
  }
}
