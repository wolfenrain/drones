import 'package:flame/extensions.dart';
import 'dart:math' as math;

extension Vector2Extension on Vector2 {
  multiplyScalar(double scalar) {
    x *= scalar;
    y *= scalar;
  }

  divideScalar(double scalar) {
    multiplyScalar(1 / scalar);
  }

  void setLength(double length) {
    normalize();
    multiplyScalar(length);
  }

  void clampLength(double min, double max) {
    final length = this.length;
    this / (length == 0 ? 1 : length) * (math.max(min, math.min(max, length)));
  }
}
