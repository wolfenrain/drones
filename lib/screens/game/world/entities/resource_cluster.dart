import 'dart:math';

import 'package:drone_game/screens/game/game.dart';
import 'package:flame/components.dart';
import 'package:flame/extensions.dart';
import 'package:flame/geometry.dart';
import 'package:flutter/material.dart';

enum ResourceType {
  nivrum,
  blentum,
  palidiam,
}

class ResourceCluster extends PositionComponent
    with HasGameRef<World>, Info, Tappable, Selectable {
  ResourceCluster({
    required this.type,
    required Vector2 position,
    required double radius,
  })  : _rotateDirection = Random().nextBool() ? 1 : -1,
        super(
          position: position,
          size: Vector2.all(radius) * 2,
          anchor: Anchor.center,
        ) {
    children.register<Resource>();
    resources = children.query<Resource>();

    switch (type) {
      case ResourceType.blentum:
        _color = const Color(0xFF5800FF);
        break;
      case ResourceType.nivrum:
        _color = const Color(0xFFE900FF);
        break;
      case ResourceType.palidiam:
        _color = const Color(0xFFFFC600);
        break;
    }
    _text = TextPaint(style: TextStyle(color: _color));
    _paint = NeonComponent.paint(_color.withOpacity(.4));
  }

  final int _rotateDirection;

  final ResourceType type;
  late final List<Resource> resources;

  late final Color _color;
  late final TextPaint _text;
  late final Paint _paint;

  @override
  String get name => type.name;

  Vector2 _randomPosition() {
    final radius = size.x / 2;
    final angle = Random().nextDouble() * 2 * pi;
    final hypotenuse = sqrt(Random().nextDouble()) * radius;
    final adjacent = cos(angle) * hypotenuse;
    final opposite = sin(angle) * hypotenuse;

    return Vector2(radius - adjacent, radius - opposite);
  }

  @override
  Future<void> onLoad() async {
    final bounds = <Rect>[];

    final int sides;
    switch (type) {
      case ResourceType.blentum:
        sides = 5;
        break;
      case ResourceType.nivrum:
        sides = 3;
        break;
      case ResourceType.palidiam:
        sides = 7;
        break;
    }

    for (var i = 0; i < 10; i++) {
      var resource = Resource(
        position: _randomPosition(),
        size: Vector2.all(20),
        sides: sides,
        color: _color,
      );

      while (bounds.any((e) => e.overlaps(resource.toRect()))) {
        resource = Resource(
          position: _randomPosition(),
          size: Vector2.all(20),
          sides: sides,
          color: _color,
        );
      }
      bounds.add(resource.toRect());
      add(resource);
    }
  }

  @override
  void update(double dt) {
    super.update(dt);

    angle += (degrees2Radians * (2 * dt)) * _rotateDirection;
  }

  Resource? mine() {
    if (resources.isNotEmpty) {
      return resources[Random().nextInt(resources.length)];
    } else {
      removeFromParent();
    }
  }

  @override
  Paint get infoPaint => _paint;

  @override
  TextPaint get infoTextPaint => _text;

  @override
  bool get displayInfo => gameRef.selected == this;

  @override
  Map<String, dynamic> infoText() {
    return {
      if (isSelected) 'Resources': children.length,
    };
  }
}

class Resource extends NeonComponent {
  Resource({
    required Vector2 position,
    required Vector2 size,
    required Color color,
    required int sides,
  }) : super(
          position: position,
          size: size,
          color: color,
          shape: HitboxPolygon([
            for (var i = 0; i < sides; i++)
              Vector2(cos(_tau / sides * i), sin(_tau / sides * i))
          ]),
        );

  static const _tau = 2 * pi;

  // Resources dont ever have to be "debugged".
  @override
  bool get debugMode => false;

  @override
  void update(double dt) {
    super.update(dt);

    angle += degrees2Radians * (20 * dt);
  }
}
