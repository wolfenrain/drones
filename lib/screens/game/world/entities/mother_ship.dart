import 'package:drone_game/screens/game/game.dart';
import 'package:drone_game/screens/game/world/mixins/steering.dart';
import 'package:flame/components.dart';
import 'package:flame/extensions.dart';
import 'package:flame/geometry.dart';
import 'package:flutter/material.dart';

final _size = Vector2(124, 124);

final _form = [
  Vector2(7, -74),
  Vector2(11, -63),
  Vector2(13, -55),
  Vector2(31, -55),
  Vector2(43, -50),
  Vector2(117, -60),
  Vector2(120, -65),
  Vector2(124, -60),
  Vector2(124, -29),
  Vector2(48, 38),
  Vector2(48, 62),
  Vector2(44, 68),
  Vector2(32, 68),
  Vector2(32, 52),
  Vector2(17, 62),
  Vector2(10, 62),
  Vector2(10, 78),
  Vector2(0, 78),
].map((p) => p..divide(_size)).toList();

class MotherShip extends NeonComponent
    with HasGameRef<World>, Info, Tappable, Selectable, Steering {
  MotherShip({
    required Vector2 position,
    Color color = const Color(0xFF4fFF69),
  }) : super(
          position: position,
          size: Vector2.all(400),
          color: color,
          shape: HitboxPolygon([
            ..._form,
            ..._form.reversed.map((p) => Vector2(-p.x, p.y)),
          ]),
          priority: 1,
          angle: 32 * degrees2Radians,
        ) {
    _text = TextPaint(style: TextStyle(color: color));
    _paint = NeonComponent.paint(color.withOpacity(.4));
  }

  late final Paint _paint;
  late final TextPaint _text;

  @override
  Future<void>? onLoad() {
    final center = size / 2;
    add(_Turret(
      position: Vector2(center.x - 90, center.y + 30),
      angles: Vector2(0, 90 * degrees2Radians),
    ));
    add(_Turret(
      position: Vector2(center.x + 90, center.y + 30),
      angles: Vector2(0, -90 * degrees2Radians),
    ));
  }

  @override
  double get maxSpeed => 100;

  @override
  String get name => 'MotherShip <${hashCode.toRadixString(16)}>';

  @override
  Paint get infoPaint => _paint;

  @override
  TextPaint get infoTextPaint => _text;

  @override
  bool get displayInfo => isSelected;

  @override
  Map<String, dynamic> infoText() {
    return {
      'Velocity': '[${velocity.x.toInt()}, ${velocity.y.toInt()}]',
    };
  }
}

class _Turret extends NeonComponent with HasGameRef<World> {
  _Turret({
    required Vector2 position,
    required Vector2 angles,
    Color color = const Color(0xFF4f69FF),
  })  : _searchingTween = Tween(begin: angles.x, end: angles.y),
        super(
          position: position,
          size: Vector2.all(30),
          color: color,
          shape: HitboxCircle(normalizedRadius: 1),
          angle: 45 * degrees2Radians,
          priority: 1,
        );

  final Tween<double> _searchingTween;

  @override
  Future<void>? onLoad() {
    final center = size / 2;
    add(NeonComponent(
      position: Vector2(
        center.x,
        size.y - 5,
      ),
      size: Vector2(10, 55),
      color: neonPaint.color,
      shape: HitboxRectangle(relation: Vector2(0.5, 0.5)),
    ));
  }

  double _delta = 0;
  bool reverse = false;

  @override
  void update(double dt) {
    _delta += (dt * (reverse ? -1 : 1)) * 0.25;
    if (_delta >= 1) {
      reverse = true;
    } else if (_delta <= 0) {
      reverse = false;
    }
    angle = _searchingTween.transform(_delta);
  }
}
