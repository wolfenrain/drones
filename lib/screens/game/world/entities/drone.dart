import 'package:drone_game/screens/game/game.dart';
import 'package:drone_game/screens/game/world/mixins/steering.dart';
import 'package:flame/components.dart';
import 'package:flame/extensions.dart';
import 'package:flame/geometry.dart';
import 'package:flutter/material.dart';

class Drone extends NeonComponent
    with HasGameRef<World>, Info, Tappable, Selectable, Steering {
  Drone({
    required Vector2 position,
    Color color = const Color(0xFFff4f69),
  })  : _movingTo = position.clone(),
        super(
          position: position,
          size: Vector2.all(20),
          color: color,
          shape: HitboxPolygon([
            Vector2(0, -1),
            Vector2(0.75, 1),
            Vector2(0, 0.75),
            Vector2(-0.75, 1),
          ]),
          priority: 1,
        ) {
    _text = TextPaint(style: TextStyle(color: color));
    _paint = NeonComponent.paint(color.withOpacity(.4));
  }

  late final Paint _paint;
  late final TextPaint _text;

  double _delta = 0;
  final Vector2 _movingTo;

  @override
  double get maxSpeed => 100;

  @override
  String get name => 'Drone <${hashCode.toRadixString(16)}>';

  ScriptComponent get _script => (children.first as ScriptComponent);
  String get script => _script.script;
  set script(String v) => _script.script = v;

  double get scanRange => 200;
  var _scanPulse = 0.0;
  bool _scanning = false;
  Tween<double>? _scanTween;

  @override
  Future<void> onLoad() async {
    add(ScriptComponent(script: '''
import 'module:core';
import 'module:math';

var clusters: List<ResourceCluster>;

fun process(drone: Drone) {
  //drone.moveTo(Vector2(500.0, -200.0));
  return;
    if (clusters == null || clusters.isEmpty) {
        clusters = drone.scan();
        drone.moveTo(Vector2(drone.position.x -50.0, drone.position.y - 12.5));
    } else {
        final cluster = clusters.first;
        if (drone.position != cluster.position) {
            drone.moveTo(cluster.position);
        } else {
            drone.mine(cluster);
            if (cluster.resources == 0) {
                clusters.remove(cluster);
                if (clusters.isEmpty) {
                    clusters = null
                }
            }
        }
    }
}
'''));
  }

  @override
  void renderDebugMode(Canvas canvas) {
    canvas
      ..save()
      ..transform(Matrix4.inverted(transformMatrix).storage)
      ..drawCircle(center.toOffset(), scanRange, debugPaint)
      ..restore();

    super.renderDebugMode(canvas);
  }

  @override
  void render(Canvas canvas) {
    super.render(canvas);

    if (_scanning && _scanPulse < scanRange) {
      canvas
        ..save()
        ..transform(Matrix4.inverted(transformMatrix).storage)
        ..drawCircle(
          center.toOffset(),
          _scanPulse,
          NeonComponent.paint(Colors.white.withOpacity(0.2), strokeWidth: 4),
        )
        ..restore();
    }
  }

  @override
  void update(double dt) {
    _delta += dt;
    if (_delta >= 1) {
      // Reset all values.
      _delta = 0;
      _scanning = false;
      _movingTo.setFrom(position);

      _onProcessTick();
    }
    if (_scanning) {
      _scanPulse = _scanTween!.transform(_delta);
    }

    if (absolutePosition.distanceTo(_movingTo) > .5) {
      seek(_movingTo);
      avoid(gameRef.children.whereType<PositionComponent>().toList());
      lookWhereGoing(smoothing: true);
    } else {
      avoid(gameRef.children.whereType<PositionComponent>().toList());
      lookWhereGoing(smoothing: true);
    }

    super.update(dt);
  }

  /// Process a single tick.
  void _onProcessTick() {
    _script.onProcessTick();

    // final clusters = _scan();
    // final inside = clusters.any((c) => c.toRect().contains(center.toOffset()));
    // if (inside) {
    //   // print('damage!');
    // }
  }

  List<ResourceCluster> _scan() {
    final range = Rect.fromCircle(center: center.toOffset(), radius: scanRange);
    return gameRef.clusters.where((e) => e.toRect().overlaps(range)).toList();
  }

  List<ResourceCluster> scan() {
    _scanning = true;
    _scanTween ??= Tween(begin: 0, end: scanRange);
    return _scan();
  }

  void moveTo(Vector2 location) {
    if (absolutePosition.distanceToSquared(location) > .5 * .5) {
      // lookAt(location);
      _movingTo.setFrom(location);
    }
  }

  void mine(ResourceCluster cluster) {
    final resource = cluster.mine();
    if (resource != null) {
      cluster.children.remove(resource);
    }
  }

  @override
  Paint get infoPaint => _paint;

  @override
  TextPaint get infoTextPaint => _text;

  @override
  bool get displayInfo => isSelected;

  @override
  Map<String, dynamic> infoText() {
    return {
      'Scan range': scanRange,
      'Velocity': '[${velocity.x.toInt()}, ${velocity.y.toInt()}]',
    };
  }
}
