import 'package:drone_game/screens/game/game.dart';
import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flame/input.dart';
import 'package:flutter/material.dart';

class World extends FlameGame with HasDraggables, HasTappables {
  Vector2 position = Vector2.zero();

  Drone? selectedDrone;

  Selectable? selected;

  late final List<Drone> drones;

  late final List<ResourceCluster> clusters;

  @override
  Color backgroundColor() => const Color(0xFF080711);

  World() {
    children.register<Drone>();
    children.register<ResourceCluster>();

    drones = children.query<Drone>();
    clusters = children.query<ResourceCluster>();
  }

  @override
  Future<void> onLoad() async {
    await super.onLoad();

    camera.followVector2(position);

    add(Drone(position: Vector2(200, 0)));
    add(MotherShip(position: Vector2.zero()));

    add(ResourceCluster(
      position: Vector2(210, 40),
      radius: 100,
      type: ResourceType.blentum,
    ));

    add(ResourceCluster(
      position: Vector2(210, -210),
      radius: 100,
      type: ResourceType.nivrum,
    ));

    add(ResourceCluster(
      position: Vector2(410, -60),
      radius: 100,
      type: ResourceType.palidiam,
    ));
  }

  bool wasNotHandledByChild = false;
  bool tapDownWasNotHandled = false;

  @override
  bool propagateToChildren<T extends Component>(
    bool Function(T) handler, {
    bool includeSelf = false,
  }) {
    return wasNotHandledByChild = super.propagateToChildren(
      handler,
      includeSelf: false,
    );
  }

  @override
  void onTapDown(int pointerId, TapDownInfo info) {
    super.onTapDown(pointerId, info);
    tapDownWasNotHandled = wasNotHandledByChild;
  }

  @override
  void onTapUp(int pointerId, TapUpInfo info) {
    super.onTapUp(pointerId, info);
    if (tapDownWasNotHandled) {
      selected = null;
    }
  }

  @override
  void onDragUpdate(int pointerId, DragUpdateInfo details) {
    super.onDragUpdate(pointerId, details);
    if (wasNotHandledByChild) {
      position.sub(details.delta.viewport);
    }
  }
}
