import 'package:hetu_script/binding.dart';
import 'package:hetu_script/hetu_script.dart';

class ResourceBinding extends HTExternalClass {
  ResourceBinding() : super('Resource');

  @override
  dynamic memberGet(String varName, {String? from}) {
    switch (varName) {
      default:
        throw HTError.undefined(varName);
    }
  }

  @override
  dynamic instanceMemberGet(dynamic object, String varName) {
    switch (varName) {
      default:
        throw HTError.undefined(varName);
    }
  }

  @override
  void instanceMemberSet(dynamic object, String varName, dynamic varValue) {
    switch (varName) {
      default:
        throw HTError.undefined(varName);
    }
  }
}
