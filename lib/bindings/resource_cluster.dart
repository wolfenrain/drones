import 'package:drone_game/screens/game/game.dart';
import 'package:hetu_script/binding.dart';
import 'package:hetu_script/hetu_script.dart';

class ResourceClusterBinding extends HTExternalClass {
  ResourceClusterBinding() : super('ResourceCluster');

  @override
  dynamic memberGet(String varName, {String? from}) {
    switch (varName) {
      default:
        throw HTError.undefined(varName);
    }
  }

  @override
  dynamic instanceMemberGet(dynamic object, String varName) {
    final cluster = object as ResourceCluster;
    switch (varName) {
      case 'position':
        return cluster.center;
      case 'resources':
        return cluster.resources.length;
      default:
        throw HTError.undefined(varName);
    }
  }

  @override
  void instanceMemberSet(dynamic object, String varName, dynamic varValue) {
    switch (varName) {
      case 'position':
      case 'resources':
        throw HTError.setterArity();
      default:
        throw HTError.undefined(varName);
    }
  }
}
