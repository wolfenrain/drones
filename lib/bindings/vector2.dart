import 'package:flame/extensions.dart';
import 'package:hetu_script/binding.dart';
import 'package:hetu_script/hetu_script.dart';

class _Vector2Binding extends HTExternalClass {
  _Vector2Binding(String id) : super(id);

  @override
  dynamic memberGet(String varName, {String? from}) {
    switch (varName) {
      case 'Vector2':
        return (
          HTEntity entity, {
          List<dynamic> positionalArgs = const [],
          Map<String, dynamic> namedArgs = const {},
          List<HTType> typeArgs = const [],
        }) =>
            Vector2(positionalArgs[0], positionalArgs[1]);

      default:
        throw HTError.undefined(varName);
    }
  }

  @override
  dynamic instanceMemberGet(dynamic object, String varName) {
    final vector2 = object as Vector2;
    switch (varName) {
      case 'x':
        return vector2.x;
      case 'y':
        return vector2.y;
      default:
        throw HTError.undefined(varName);
    }
  }

  @override
  void instanceMemberSet(dynamic object, String varName, dynamic varValue) {
    final vector2 = object as Vector2;
    switch (varName) {
      case 'x':
        vector2.x = varValue;
        break;
      case 'y':
        vector2.y = varValue;
        break;
      default:
        throw HTError.undefined(varName);
    }
  }
}

class Vector2Binding extends _Vector2Binding {
  Vector2Binding() : super('Vector2');
}

class NotifyingVector2Binding extends _Vector2Binding {
  NotifyingVector2Binding() : super('NotifyingVector2');
}
