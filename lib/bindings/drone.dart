import 'package:drone_game/screens/game/game.dart';
import 'package:hetu_script/binding.dart';
import 'package:hetu_script/hetu_script.dart';

class DroneBinding extends HTExternalClass {
  DroneBinding() : super('Drone');

  @override
  dynamic memberGet(String varName, {String? from}) {
    switch (varName) {
      default:
        throw HTError.undefined(varName);
    }
  }

  @override
  dynamic instanceMemberGet(dynamic object, String varName) {
    final drone = object as Drone;
    switch (varName) {
      case 'position':
        return drone.center;
      case 'moveTo':
        return (
          HTEntity entity, {
          List<dynamic> positionalArgs = const [],
          Map<String, dynamic> namedArgs = const {},
          List<HTType> typeArgs = const [],
        }) =>
            drone.moveTo(positionalArgs.first);
      case 'scan':
        return (
          HTEntity entity, {
          List<dynamic> positionalArgs = const [],
          Map<String, dynamic> namedArgs = const {},
          List<HTType> typeArgs = const [],
        }) =>
            drone.scan();
      case 'mine':
        return (
          HTEntity entity, {
          List<dynamic> positionalArgs = const [],
          Map<String, dynamic> namedArgs = const {},
          List<HTType> typeArgs = const [],
        }) =>
            drone.mine(positionalArgs.first);
      default:
        throw HTError.undefined(varName);
    }
  }

  @override
  void instanceMemberSet(dynamic object, String varName, dynamic varValue) {
    final drone = object as Drone;
    switch (varName) {
      case 'position':
        drone.center = varValue;
        break;
      case 'moveTo':
      case 'scan':
        throw HTError.notCallable(varName);
      default:
        throw HTError.undefined(varName);
    }
  }
}
