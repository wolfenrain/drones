import 'package:drone_game/screens/game/game.dart';
import 'package:flame/flame.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Flame.device.fullScreen();
  runApp(const App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final baseTheme = ThemeData.light();
    return MaterialApp(
      title: 'Drones',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        floatingActionButtonTheme: const FloatingActionButtonThemeData(
          backgroundColor: Color(0xFF141a1f),
        ),
        textTheme: baseTheme.textTheme.apply(
          bodyColor: const Color(0xFF9c9da1),
        ),
        iconTheme: baseTheme.iconTheme.copyWith(
          color: const Color(0xFF9c9da1),
        ),
        dividerColor: const Color(0xFF1e242b),
      ),
      debugShowCheckedModeBanner: false,
      home: const GamePage(),
    );
  }
}
