
import 'package:code_text_field/code_text_field.dart';
import 'package:flutter/material.dart';

class CurlyBlockModifier extends CodeModifier {
  const CurlyBlockModifier() : super('{');

  @override
  TextEditingValue? updateString(
    String text,
    TextSelection sel,
    EditorParams params,
  ) {
    return TextEditingValue(
      text: text.replaceRange(sel.start, sel.end, '{}'),
      selection: TextSelection(
        baseOffset: sel.start + 1,
        extentOffset: sel.start + 1,
      ),
    );
  }
}
